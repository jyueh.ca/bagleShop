---
title: "About"
date: "2016-05-05T21:48:51-07:00"
---

Welcome to my bagle shop, which is undergoing a lot of changes. Later I'd like to change the theme for my shop.
[**blogdown**](https://github.com/rstudio/blogdown) package. The theme was forked from [@jrutheiser/hugo-lithium-theme](https://github.com/jrutheiser/hugo-lithium-theme) and modified by [Yihui Xie](https://github.com/yihui/hugo-lithium-theme).
